var x = 6
var y = 14
var z =4 

x += y - x++ * z;
document.write(x +"<br/>");
/* Спочатку виконується x++ * z що буде дорівнювати 24 (вищiй прiорiтет), 
після цього y - x++ * z що дорівнює -10 бо у - приорiтет вище нiж у +=, 
потім 6+(-10)= -4. 
Відповідь: х = -4*/

var x = 6
var y = 14
var z =4 

z = --x - y * 5 ;
document.write(z +"<br/>");
/* Спочатку іде дія -- як пріорітетна, через це х тепер = 5,
далi виконуємо множення y * 5 = 70, 
а після цього віднімання 5 - 70 = -65,
(z = 5 - 14 * 5)
Вiдповiдь: z = -65 */

var x = 6
var y = 14
var z =4 

y /= x + 5 % z; 
document.write(y +"<br/>");

/* Спочатку рахуємо залишок 5 % z = 5 % 4 = 1, далi виконуємо таке наче рівняння
x + 1 (6+1=7),
y /= 7, 
14/7=2,
Вiдповiдь : y = 2 */

var x = 6
var y = 14
var z =4 

result = z - x++ + y * 5;
document.write(result +"<br/>");
/* Спочатку віднімання z - x++ (у нас постфіксний інкремент, тому х++ = 6) = -2, 
після цього множення y * 5 = 70,
наприкінці -2 + 70 = 68,
Вiдповiдь: 68 */

var x = 6
var y = 14
var z =4 

x = y - x++ * z;
document.write(x +"<br/>") ;
/* Спочатку виконуємо x++ * z, інкремент у нас постфіксний, тому х++ = 6 (6*4=24),
потім віднімання y - 24 = 14 - 24 = x = -10,
Вiдповiдь: -10 */